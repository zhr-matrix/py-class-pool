.. py-class-pool documentation master file, created by
   sphinx-quickstart on Tue Jul  3 22:55:03 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Getting start
=============

py-class-pool is a simple but powerful class registry for Python with additional Django support. It allows
to build dynamic registry of classes in a similar way how Django handles registry of models. Classes are distinguished
by ID.

.. warning::
    This library is used on production environment however please note that it has been not used widely in
    different use cases. Please consider this before using it on production environment. Any issue reports
    and improvement suggestions are welcomed.



Installation
------------

::

   pip install class-pool

Features
--------

* register classes in four ways:

   - on-demand using register() method
   - using register() decorator
   - by metaclass
   - by scanning list of provided packages/modules
* access classes by ID
* **each registry is a singleton**
* define custom ID generator
* specify if abstract classes should be registered or not
* listen for on register signal to run additional logic when class is registered
* Django support which includes loading classes from applications, choice field which allows to select registered classes
  and corresponding form field
* Django REST Framework support with serializer field and dynamic serializers loaded from a pool
* and many more

Requirements
------------

* Python 3
* Django to use django related features (optional)
* Django REST Framework to use DRF related features (optional)

Contribution
------------

Library is developed and maintained by `The Matrix Team <https://matrix.zhr.pl>`_,
IT team of `The Scouts Organisation of the Republic of Poland <http://zhr.pl>`_.

Source code is available at `GitLab <https://gitlab.com/zhr-matrix/py-class-pool>`_.
Everyone can contribute to this library by making a PR, reporting a bug or suggesting an improvement.


.. toctree::
   :maxdepth: 2
   :caption: Basics:

   basics

.. toctree::
   :maxdepth: 2
   :caption: Advanced topics:

   decorator
   customid
   filtering
   metaclass
   autoscanning
   signal
   singleton

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
