# class-pool

Simple, but powerful library for python classes registries.

This tool bases on django's models pool with a lot of improvements.

*Features*
- thread-safe classes pool
- ad-hoc class registering
- auto class registering from packages
- custom class identifiers in a pool
- filtering classes during registration
- default class for unregistered identifiers
- pool classes choice field for Django models, forms and Django Rest Framework serializers

## Requirements
* Python 3
* django >= 1.7 (optional)

## Installation

``pip install class-pool``

## Documentation

https://py-class-pool.readthedocs.io/

