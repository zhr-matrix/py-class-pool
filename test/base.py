# -*- coding: utf-8 -*-

"""Created on 22.06.18

.. moduleauthor:: Paweł Pecio
"""
import unittest

from class_pool.pool import Pool


class PoolTestCase(unittest.TestCase):

    def create_pool(self):
        return Pool()

    def setUp(self):
        self.pool = self.create_pool()

    def tearDown(self):
        # clear thread-local shared-state
        lock = Pool._Pool__shared_state['lock']
        counter = Pool._Pool__shared_state['counter']
        Pool._Pool__shared_state.clear()
        Pool._Pool__shared_state['lock'] = lock
        Pool._Pool__shared_state['counter'] = counter
        Pool._registry = {}
